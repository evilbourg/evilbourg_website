<?php
/**
 * Created by PhpStorm.
 * User: MATASSE
 * Date: 23/02/2019
 * Time: 13:22
 */

namespace App\Service;


class UserRankService
{

    public const ADMIN_RANK = '4dmin_H3Re!';
    public const ADMIN_TEXT= 'here you are!';
    public const USER_RANK= 'Utilisateur';
    public const USER_TEXT= 1;

    function getSessionRank(){

        if(isset($_SESSION[UserRankService::ADMIN_RANK])){
            if($_SESSION[UserRankService::ADMIN_RANK] === UserRankService::ADMIN_TEXT){
                return UserRankService::ADMIN_RANK;
            }
        }

        if(isset($_SESSION[UserRankService::USER_RANK])){
            if($_SESSION[UserRankService::USER_RANK] === UserRankService::USER_TEXT){
                return UserRankService::USER_RANK;
            }
        }

        return 'Non connecté';

    }

}