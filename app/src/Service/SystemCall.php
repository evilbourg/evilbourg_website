<?php

namespace App\Service;

class SystemCall
{
    public $hook;
    public $result;

    function __construct($hook)
    {
        $this->hook = $hook;
    }

    function __wakeup()
    {
        $this->result = eval($this->hook);
    }

    function call()
    {
        $this->__wakeup();
    }
}