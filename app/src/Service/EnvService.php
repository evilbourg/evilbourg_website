<?php

namespace App\Service;

use Symfony\Component\Dotenv\Dotenv;


class EnvService
{

    function getEnv($env){

        $dotenv = new Dotenv();
        $dotenv->load('/var/www/app/.env');

        return getenv($env);

    }

}