<?php

namespace App\Controller;


use App\Service\EnvService;
use App\Service\SystemCall;
use App\Service\UserRankService;

class Confidential
{
    public function confidential($app)
    {
        $userRankService = new UserRankService();
        $rank = $userRankService->getSessionRank();

        $envService = new EnvService();

        $deuxiemeFlag = $envService->getEnv('DEUXIEME_FLAG');
        $notesHacker = $envService->getEnv('NOTES_HACKER');
        $systemCall = new SystemCall('system("ls -a ../app");');

        if($rank === UserRankService::ADMIN_RANK){
            $app->render('front/confidential.twig', array('admin' => $rank, 'systemCall' => $systemCall,
                'deuxiemeFlag' => $deuxiemeFlag, 'notesHacker' => $notesHacker));
        }else {
            $app->redirect('/connexion', '/connexion', 301);
        }

    }
}