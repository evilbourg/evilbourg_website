<?php

namespace App\Controller;


use App\Service\EnvService;
use App\Service\UserRankService;

class Contribution
{

    public function contribution($app)
    {
        $userRankService = new UserRankService();
        $rank = $userRankService->getSessionRank();

        $envService = new EnvService();

        $gitlabLink = $envService->getEnv('GITLAB_LINK');


        if($rank === UserRankService::ADMIN_RANK || $rank === UserRankService::USER_RANK){
            $app->render('front/contribution.twig', array('admin' => $rank, 'gitlabLink' => $gitlabLink));
        }else {
            $app->redirect('/connexion', '/connexion', 301);
        }

    }
}