<?php

namespace App\Controller;


use App\Service\UserRankService;
use Slim\Middleware\SessionCookie;
use Slim\Slim;

class TraitorList
{
    public function admin($app)
    {
        $userRankService = new UserRankService();
        $rank = $userRankService->getSessionRank();


        if($rank === UserRankService::ADMIN_RANK || $rank === UserRankService::USER_RANK){
            $app->render('front/traitorList.twig', array('admin' => $rank));
        }else {
            $app->redirect('/connexion', '/connexion', 301);
        }

    }
}