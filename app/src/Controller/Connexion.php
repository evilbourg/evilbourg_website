<?php
/**
 * Created by PhpStorm.
 * User: MATASSE
 * Date: 16/02/2019
 * Time: 17:46
 */

namespace App\Controller;


use App\Service\EnvService;
use App\Service\UserRankService;

class Connexion
{
    public function connexion($app)
    {

        $userRankService = new UserRankService();
        $rank = $userRankService->getSessionRank();


        $envService = new EnvService();
        $admin_name = $gitlabLink = $envService->getEnv('ADMIN_NAME');
        $admin_pass = $gitlabLink = $envService->getEnv('ADMIN_PASS');
        $user_name = $gitlabLink = $envService->getEnv('USER_NAME');
        $user_pass = $gitlabLink = $envService->getEnv('USER_PASS');

        if(isset($_POST['login']) && isset($_POST['password'])){
            if($_POST['login'] === $admin_name && $_POST['password'] === $admin_pass){
                $_SESSION[UserRankService::ADMIN_RANK] = UserRankService::ADMIN_TEXT;
                $app->redirect('/traitorList', '/traitorList', 301);
            }

            if($_POST['login'] === $user_name && $_POST['password'] === $user_pass){
                $_SESSION[UserRankService::USER_RANK] = UserRankService::USER_TEXT;
                $app->redirect('/traitorList', '/traitorList', 301);
            }

        }

        if(!isset($_SESSION[UserRankService::USER_RANK]) && !isset($_SESSION[UserRankService::ADMIN_RANK])){
            $_SESSION[UserRankService::USER_RANK] = 0;
        }

        $app->render('front/admin_connexion.twig', array('admin' => $rank));
    }

    public function deconnexion($app)
    {

        $_SESSION = array();

        $app->redirect('/connexion', '/connexion', 301);
    }

}