<?php

namespace App\Controller;

use App\Service\UserRankService;

class Front
{

    public function home($app)
    {

        $userRankService = new UserRankService();
        $rank = $userRankService->getSessionRank();

        $app->render('front/home.twig', array('admin' => $rank));
    }
}
