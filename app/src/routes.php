<?php

// Routes

$app->get('/', 'App\Controller\Front:home')
    ->name('home')
    ->setParams([$app]);

$app->get('/connexion', 'App\Controller\Connexion:connexion')
    ->name('connexion')
    ->setParams([$app]);

$app->post('/connexion', 'App\Controller\Connexion:connexion')
    ->name('connexion_post')
    ->setParams([$app]);

$app->get('/deconnexion', 'App\Controller\Connexion:deconnexion')
    ->name('deconnexion')
    ->setParams([$app]);

$app->get('/contribution', 'App\Controller\Contribution:contribution')
    ->name('contribution')
    ->setParams([$app]);

$app->get('/traitorList', 'App\Controller\TraitorList:admin')
    ->name('traitorList')
    ->setParams([$app]);

$app->get('/confidential', 'App\Controller\Confidential:confidential')
    ->name('confidential')
    ->setParams([$app]);


// Page non trouvee

$app->notFound(function () use ($app) {
    $app->render('errors/not-found.twig');
});
