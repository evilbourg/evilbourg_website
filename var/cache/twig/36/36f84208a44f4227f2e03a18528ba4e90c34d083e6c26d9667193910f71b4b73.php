<?php

/* front/confidential.twig */
class __TwigTemplate_50b70e3120b7813f6cda0700dd481e6fc29af8bd0b69d845cfa21a35092f9538 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layouts/main.twig", "front/confidential.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "layouts/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <h1>Liste des fichiers confidentiels</h1>

    <p>Cette partie n'est plus un secret pour vous, vous êtes maintenant au même point que Kantino ! </p>
    <p>Deuxième flag : ";
        // line 7
        echo twig_escape_filter($this->env, ($context["deuxiemeFlag"] ?? $this->getContext($context, "deuxiemeFlag")), "html", null, true);
        echo "</p>

    <h2>Notes du dernier Hacker</h2>
    <p class=\"contribution-note\">Cette demande a été faite par notre chef suprême, nous ne sommes pas responsables des effets collatéraux de cette décision!</p>

    <p>";
        // line 12
        echo twig_escape_filter($this->env, ($context["notesHacker"] ?? $this->getContext($context, "notesHacker")), "html", null, true);
        echo "</p>

    <h2>fichiers confidentiels</h2>
    <p>Ici sont listés les fichiers confidentiels présents sur ce serveur, connectez-vous au serveur pour les consulter</p>

    <p class=\"contribution-note\">Cette demande a également été faite par notre chef suprême, si quelqu'un voit ceci aidez nous, il est bien trop stupide pour gouverner le monde!</p>

    <code>
        ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["systemCall"] ?? $this->getContext($context, "systemCall")), "call", []), "html", null, true);
        echo "
    </code>

";
    }

    public function getTemplateName()
    {
        return "front/confidential.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 20,  44 => 12,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "front/confidential.twig", "/var/www/app/templates/front/confidential.twig");
    }
}
