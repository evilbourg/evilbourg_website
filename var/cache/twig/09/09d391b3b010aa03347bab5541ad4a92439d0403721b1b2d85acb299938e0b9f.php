<?php

/* front/home.twig */
class __TwigTemplate_2e75bf1ee6334511c8d626cd70997a76a973469c1e398b7d920035116fd8ea64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layouts/main.twig", "front/home.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "layouts/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "

    <h1>Site de la prison EvilBourg</h1>

    <p>Les hérétiques envers le gouvernement sont envoyés dans cette prison, seuls les employés abilités sont autorisés à accéder à ce site</p>
    
    <img src=\"assets/img/prison.jpg\">
";
    }

    public function getTemplateName()
    {
        return "front/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "front/home.twig", "/var/www/app/templates/front/home.twig");
    }
}
