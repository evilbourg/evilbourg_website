<?php

/* front/admin_connexion.twig */
class __TwigTemplate_4728fb6c5cc35373b155dd5493284aaae22e4d2036d0936d4009f2c1a3a42100 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layouts/main.twig", "front/admin_connexion.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "layouts/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <h1>Connexion</h1>

    <p>Connectez-vous en utilisant ce formulaire, toute tentative non autorisée sera punie! Le dernier à avoir pénetré cette application a été exécuté avant-hier!</p>

    <form method=\"post\">
        <div class=\"form-group\">
            <label for=\"login\">Login</label>
            <input class=\"form-control\" id=\"login\" aria-describedby=\"login\" placeholder=\"Login\" name=\"login\">
        </div>
        <div class=\"form-group\">
            <label for=\"password\">Mot de passe</label>
            <input type=\"password\" class=\"form-control\" id=\"password\" aria-describedby=\"password\" placeholder=\"Mot de passe\" name=\"password\">
        </div>
        <button type=\"submit\" class=\"btn btn-primary\">Connexion</button>
    </form>

";
    }

    public function getTemplateName()
    {
        return "front/admin_connexion.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "front/admin_connexion.twig", "/var/www/app/templates/front/admin_connexion.twig");
    }
}
