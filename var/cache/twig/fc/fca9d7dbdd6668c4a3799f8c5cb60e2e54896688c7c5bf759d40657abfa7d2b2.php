<?php

/* layouts/main_error.twig */
class __TwigTemplate_cf6bf497937307f0128b5a4c41200540b63bca7e7835ac8b0d2af63e97de8bd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
    <meta charset=\"utf-8\">

    <link rel=\"stylesheet\" href=\"assets/css/main.css\">
    <link rel=\"stylesheet\" href=\"assets/css/bootstrap.min.css\">
    <link rel=\"icon\" href=\"assets/img/icone.png\">
</head>
<body>

<nav class=\"navbar navbar-dark bg-dark justify-content-between\">
    <a class=\"navbar-brand\" href=\"/\">Home</a>

</nav>
<div class=\"container\">
    ";
        // line 17
        $this->displayBlock('content', $context, $blocks);
        // line 18
        echo "</div>

<script src=\"assets/js/jquery.min.js\"></script>
<script src=\"assets/js/main.js\"></script>
<script src=\"assets/js/bootstrap.min.js\"></script>
</body>
</html>";
    }

    // line 17
    public function block_content($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "layouts/main_error.twig";
    }

    public function getDebugInfo()
    {
        return array (  50 => 17,  40 => 18,  38 => 17,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "layouts/main_error.twig", "/var/www/app/templates/layouts/main_error.twig");
    }
}
