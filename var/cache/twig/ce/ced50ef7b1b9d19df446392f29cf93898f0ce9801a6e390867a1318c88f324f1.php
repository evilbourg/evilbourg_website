<?php

/* layouts/main.twig */
class __TwigTemplate_e925092a99a90b763270fe8f1d6f2528ddedb35e0ad37bd4c8bdefb004b0c0a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!doctype html>
<html>
    <head>
        <meta charset=\"utf-8\">

        <link rel=\"stylesheet\" href=\"assets/css/main.css\">
        <link rel=\"stylesheet\" href=\"assets/css/bootstrap.min.css\">
        <link rel=\"icon\" href=\"assets/img/icone.png\">
    </head>
    <body>

        <nav class=\"navbar navbar-dark bg-dark justify-content-between\">
            <a class=\"navbar-brand\" href=\"/\">EvilBourg</a>
            <ul class=\"nav\">

                ";
        // line 16
        if ((($context["admin"] ?? $this->getContext($context, "admin")) == twig_constant("App\\Service\\UserRankService::ADMIN_RANK"))) {
            echo "<li><a class=\"nav-item nav-link\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Slim\Views\TwigExtension')->urlFor("confidential"), "html_attr");
            echo "\">Confidentiel</a></li> ";
        }
        // line 17
        echo "                ";
        if (((($context["admin"] ?? $this->getContext($context, "admin")) == twig_constant("App\\Service\\UserRankService::USER_RANK")) || (($context["admin"] ?? $this->getContext($context, "admin")) == twig_constant("App\\Service\\UserRankService::ADMIN_RANK")))) {
            echo "<li><a class=\"nav-item nav-link\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Slim\Views\TwigExtension')->urlFor("contribution"), "html_attr");
            echo "\">Contribution</a></li> ";
        }
        // line 18
        echo "                ";
        if (((($context["admin"] ?? $this->getContext($context, "admin")) == twig_constant("App\\Service\\UserRankService::USER_RANK")) || (($context["admin"] ?? $this->getContext($context, "admin")) == twig_constant("App\\Service\\UserRankService::ADMIN_RANK")))) {
            echo "<li><a class=\"nav-item nav-link\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Slim\Views\TwigExtension')->urlFor("traitorList"), "html_attr");
            echo "\">Liste hérétiques</a></li> ";
        }
        // line 19
        echo "                ";
        if (((($context["admin"] ?? $this->getContext($context, "admin")) == twig_constant("App\\Service\\UserRankService::USER_RANK")) || (($context["admin"] ?? $this->getContext($context, "admin")) == twig_constant("App\\Service\\UserRankService::ADMIN_RANK")))) {
            echo "<li><a class=\"nav-item nav-link\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Slim\Views\TwigExtension')->urlFor("deconnexion"), "html_attr");
            echo "\">Deconnexion</a></li>
                ";
        } else {
            // line 20
            echo "<li><a class=\"nav-item nav-link\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Slim\Views\TwigExtension')->urlFor("connexion"), "html_attr");
            echo "\">Connexion</a></li>";
        }
        // line 21
        echo "                <label class=\"nav-item nav-link\">";
        echo twig_escape_filter($this->env, ($context["admin"] ?? $this->getContext($context, "admin")), "html", null, true);
        echo "</label>
            </ul>

        </nav>
        <div class=\"container\">
            ";
        // line 26
        $this->displayBlock('content', $context, $blocks);
        // line 27
        echo "        </div>

        <script src=\"assets/js/jquery.min.js\"></script>
        <script src=\"assets/js/main.js\"></script>
        <script src=\"assets/js/bootstrap.min.js\"></script>
    </body>
</html>";
    }

    // line 26
    public function block_content($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "layouts/main.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 26,  81 => 27,  79 => 26,  70 => 21,  65 => 20,  57 => 19,  50 => 18,  43 => 17,  37 => 16,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "layouts/main.twig", "/var/www/app/templates/layouts/main.twig");
    }
}
