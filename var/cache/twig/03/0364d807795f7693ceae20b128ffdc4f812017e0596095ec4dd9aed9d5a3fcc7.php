<?php

/* front/traitorList.twig */
class __TwigTemplate_32acd875866e7a1223cf8b8cbee364e8ef79181b52f7e652eb85983c601f21e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layouts/main.twig", "front/traitorList.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "layouts/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <h1>Liste des hérétiques</h1>


    <div class=\"container traitor-list\">

        <div class=\"traitor-elem\">
            <h2>Kantino</h2>
            <img src=\"assets/img/kantino.jpg\">
            <p>Un hacker repéré proche de l'enceinte de la prison, il a réussi à se faufiler dans notre réseau, heureusement le directeur d'Evilbourg a réussi à récupérer les informations qu'il devait envoyer à son équipe de CTF... Nous avons eu beaucoup de chance, il semblerait qu'il ait été très proche de compromettre ce serveur et de découvrir le dossier contenant nos photos personnelles. Pour punir cet acte effronté, kantino a été torturé pendant une semaine puis exécuté!</p>

        </div>

        <div class=\"traitor-elem\">
            <h2>Canard mandarin</h2>
            <img src=\"assets/img/canard-mandarin.jpg\">
            <p>Un canard retrouvé dans l'enceinte de la prison, humain ou pas, nous sommes tous égaux face à la justice d'Evilbourg! Il a fini en magret dans l'heure qui a suivi sa visite.</p>

        </div>


    </div>


";
    }

    public function getTemplateName()
    {
        return "front/traitorList.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "front/traitorList.twig", "/var/www/app/templates/front/traitorList.twig");
    }
}
