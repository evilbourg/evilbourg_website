<?php

/* front/contribution.twig */
class __TwigTemplate_63bf93dddc0838fb790cdb72775a67706232ef22a3d802b9e8bdf7b94915a6d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layouts/main.twig", "front/contribution.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "layouts/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "<h1>Contribution</h1>

    <p>Vous pouvez contribuer au site d'Evilbourg en vous connectant au lien suivant : <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["gitlabLink"] ?? $this->getContext($context, "gitlabLink")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["gitlabLink"] ?? $this->getContext($context, "gitlabLink")), "html", null, true);
        echo "</a> </p>

    <p class=\"contribution-note\">(Tout employé ne contribuant pas suffisament sera considéré comme hérétique)</p>


";
    }

    public function getTemplateName()
    {
        return "front/contribution.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "front/contribution.twig", "/var/www/app/templates/front/contribution.twig");
    }
}
